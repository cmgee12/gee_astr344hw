import numpy as np

print "For x=(1/3)**n:"
#if go to n=5, single precision floating point result= 1
x=0
for i in range(0,6):
    x=x+(1/3)**i
    #print i
print "Single precision floating point to n=5:", np.float32(x)
x_n5s=np.float32(x)

#if go to n=5, double precision point result=1.49794
x=0
for i in range(0,6):
    x=x+(1./3.)**i
    #print i
print "Double precision floating point to n=5:", np.float32(x)
x_n5d=np.float32(x)

#absolute error for floating point numbers: e(x)=x-xbar
absolute_error= x_n5d - x_n5s
print "Absolute Error, n=5:", absolute_error

#relative error for floating point numbers: Er(x)=(x-xbar)/x
relative_error = (x_n5d-x_n5s)/x_n5d
print "Relative Error, n=5:", relative_error

#if go to n=20, single precision floating point number result = 1.0
x=0
for i in range(0,21):
    x=np.float64(x)+(1/3)**i
    #print i
print "Single precision floating point number to n=20:", np.float64(x)
x_n20s=np.float64(x)

#if go to n=20, double precision floating point number result = 1.49999999986
x=0
for i in range(0,21):
    x=np.float64(x)+(1./3.)**i
    #print i
print "Double precision floating point number to n=20:", np.float64(x)
x_n20d=np.float64(x)

#absolute error for floating point numbers: e(x)=x-xbar
absolute_error= x_n20d - x_n20s
print "Absolute Error, n=20:", absolute_error

#relative error for floating point numbers: Er(x)=(x-xbar)/x
relative_error = (x_n20d-x_n20s)/x_n20d
print "Relative Error, n=20:", relative_error

print "For x=4**n and x(1)=4"
#single precision for for x(1)=4 and x=4**n
x=4
for i in range(0,21):
    x=x+4**i
    #print i
print "Single precision floating point to n=20:", np.float64(x)
x_n20s=np.float64(x)

#double precision for x(1)=4 and x=4**n
x=4
for i in range(0,21):
    x=x+4.**i
    #print i
print "Double precision floating point to n=20:", np.float64(x)
x_n20d=np.float64(x)

#absolute error for floating point numbers: e(x)=x-xbar
absolute_error= x_n20d - x_n20s
print "Absolute Error, n=20:", absolute_error

#relative error for floating point numbers: Er(x)=(x-xbar)/x
relative_error = (x_n20d-x_n20s)/x_n20d
print "Relative Error, n=20:", relative_error

print "This calculation is stable because the base value of 4 is an integer that can be represented in binary numbers, whereas the base number 1/3 cannot"
print "Stability should be measured by absolute error as 0 if it is stable. Accuracy should be measured by relative error since it returns a percent error"

print "Testing single vs. double precision numbers"
x=np.float64(1.e200)
y=np.float32(10.)
print x
print y

x=np.float64(1.e200)
y=np.float32(1.e200)
print x
print "It is working since answer prints as infinity:", y