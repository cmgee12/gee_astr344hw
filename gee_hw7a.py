import numpy as numpy
from numpy import random


#Calculate pi=3.14
#Using Monte Carlo Method: A_circle= x*R**2
#generate random x and random y
#count number of points where x and y are inside circumference of circle, C=2*x*R
#Set R=1/2, xmin=0, xmax=1, ymin=0, ymax=1

#Calculating area of square
"""
#random x positions of darts
def f(x):
    return x
N=1000
for i in range(0,N): 
    I_x=2*f(numpy.random.random())-1
print "x", I_x

#random y positions of darts
def f(y):
    return y
for i in range(0, N):
    I_y=2*f(numpy.random.random())-1
print "y", I_y"""

#darts inside area of circle
N=1000
count=0
for i in range(0,N):
        if ((2.0*numpy.random.random()-1)**2+(2.0*numpy.random.random()-1)**2)<=1:
            count=count+1
print "The value of pi with N=1,000 is", (4.0*count)/N

#Monte Carlo sample 10000
N_1=10000
count=0
for i in range(0,N_1):
        if ((2.0*numpy.random.random()-1)**2+(2.0*numpy.random.random()-1)**2)<=1:
            count=count+1
print "The value of pi with N=10,000 is", (4.0*count)/N_1

#Monte Carlo sample 100,000
N_2=100000
count=0
for i in range(0,N_2):
        if ((2.0*numpy.random.random()-1)**2+(2.0*numpy.random.random()-1)**2)<=1:
            count=count+1
print "The value of pi with N=100,000 is", (4.0*count)/N_2

#Monte Carlo sample 1,000,000
N_3=1000000
count=0
for i in range(0,N_3):
        if ((2.0*numpy.random.random()-1)**2+(2.0*numpy.random.random()-1)**2)<=1:
            count=count+1
print "The value of pi with N=1,000,000 is", (4.0*count)/N_3

#Monte Carlo sample 10,000,000
N_4=10000000
count=0
for i in range(0,N_4):
        if ((2.0*numpy.random.random()-1)**2+(2.0*numpy.random.random()-1)**2)<=1:
            count=count+1
print "The value of pi with N=10,000,000 is", (4.0*count)/N_4

print "Thus, it is evident that increasing N, the number of Monte Carlo points, also increases the accuracy of calculating pi as 3.141. Depending on the random numbers, it is pretty accurate around 1E7 Monte Carlo points"
    
    
    
    
