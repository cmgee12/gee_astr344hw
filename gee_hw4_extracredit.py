#create array of z values between 0 and 5
import numpy as np
x=np.linspace(-1,1,1000)
from numpy import cumsum

def function(x):
    hold=[]
    for i in range(0, len(x)):
        x[i]=float(x[i])
        hold.append(x[i]**3+2*x[i]**2-4)
    return hold
#print function(x)
function=function(x)

def linear(x,function):
    hold=[]
    for i in range(0, len(x)-1):
        x[i]=float(x[i])
        x[i+1]=float(x[i+1])
        function[i]=float(function[i])
        hold.append(function[i]*(x[i+1]-x[i]))
    return hold
#print linear(x,function)
linear=linear(x,function)
linear_sum=cumsum(linear)
print "Piecewise linear function", linear_sum

#calculate trapezoid areas
def trap_area(x, function):
    hold=[]
    for value in range(0, len(x)-1):
        x[value]=float(x[value])
        x[value+1]=float(x[value+1])
        function[value]=float(function[value])
        function[value+1]=float(function[value+1])
        hold.append((x[value+1]-x[value])*((function[value+1]+function[value])/2))
    return hold
#print "Trapezoid areas", np.float64(trap_area(z))
trap_area=trap_area(x,function) 
trap_sum=cumsum(trap_area)
print "Trapezoidal integration", trap_sum

lin_error=((20/3)-(6.66866600))/(20/3)
trap_error=((20/3)-(6.66666399))/(20/3)

print "The exact integral of the function from -1 to 1 is -20/3 since the integral is (1/4)x**4+(2/3)x**3-4."
print "Therefore, the relative error of the linear method from -1 to 1 is", lin_error
print "And the relative error of the trapezoid method is", trap_error
        