#create array of z values between 0 and 5
import numpy as np
z=np.linspace(0,5,1000)
from numpy import cumsum


C=3*10**3
m=0.3
lamda=0.7

#calculate r(z)=C/(sqrt((m*(1+z[value])**3+lamda
from math import sqrt
def r(z):
    hold=[]
    for value in range (0, len(z)):
        z[value]=float(z[value])
        hold.append(C/(sqrt((m*(1+z[value])**3+lamda))))
    return hold
#print "R(z) values", r(z)
r=r(z)

#calculate trapezoid areas
def trap_area(z):
    hold=[]
    for value in range(0, len(z)-1):
        z[value]=float(z[value])
        z[value+1]=float(z[value+1])
        r[value]=float(r[value])
        r[value+1]=float(r[value+1])
        hold.append((z[value+1]-z[value])*((r[value+1]+r[value])/2))
    return hold
#print "Trapezoid areas", np.float64(trap_area(z))
trap_area=trap_area(z) 
trap_sum=cumsum(trap_area)
#print trap_sum

#calculate DA values
def DA(trap_sum,z):
    hold=[]
    for value in range(0, len(z)-1):
        trap_sum[value]=float(trap_sum[value])
        z[value+1]=float(z[value+1])
        hold.append(trap_sum[value]/(1+z[value+1]))
    return hold
#print np.float64(DA(trap_area, z))
DA=DA(trap_sum, z)
DA.insert(0,0)
#print DA

#plot DA(z)
import matplotlib.pyplot as plt
data=plt.plot(z, DA, 'bo', label='data')
plt.ylabel('DA')
plt.xlabel('z')
plt.axis([0, 5, 0, 1500])
plt.legend(numpoints=1)
plt.show()

from numpy import trapz

def trapezoid(r,z):
    hold=[]
    for value in (0, len(r)-1):
        r[value]=float(r[value])
        z[value]=float(z[value])
        hold.append((np.trapz(r[value]))/(1+z[value]))
    return hold
print trapezoid(r,z)






 
  
        
    