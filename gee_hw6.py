#Find roots of functions using NR method and compare to bisection method
#Part 1
import numpy as np
import math
from numpy import cumsum
from numpy import insert
from math import expm1
from math import exp

#NR method
"""x_i=np.linspace(0,5,5)
print "x_i is", x_i

# test function ((x**3)-x-2)
def function(x_i):
    hold=[]
    for value in range(0, len(x_i)):
        x_i[value]=float(x_i[value])
        hold.append((x_i[value]**3)-x_i[value]-2)
    return hold
function=function(x_i)
print "function", function

#calculate derivative using trapezoid approximation
def deriv_function(x_i, function):
    hold=[]
    for value in range(0, len(x_i)-1):
        x_i[value]=float(x_i[value])
        x_i[value+1]=float(x_i[value+1])
        function[value]=float(function[value])
        function[value+1]=float(function[value+1])
        hold.append((x_i[value+1]-x_i[value])*((function[value+1]+function[value])/2))
    return hold
#print "Trapezoid areas", np.float64(function(x_i))
deriv_function=deriv_function(x_i,function)
deriv_function.insert(0,.01)
trap_sum=cumsum(deriv_function) 
print "Derivative Trap_sum", trap_sum

#calculate Newton-Rapshun approximation x_(n+1)=x_n-f(x_n)/f'(x_n)

del function[0] #to make matrix correct dimensions
function1=function
print "function1", function1

def NR(x_i,function, trap_sum):
    tol=.1
    NR=1
    while (NR<=100):
        x1=(x_i-(function/trap_sum))
        if all(x1-x_i < tol):
            return x1
        else:
            x_i=x1
    return False
print NR(x_i,function, trap_sum)"""

#Newton Raphson Method

#define Newton Rasphon approximation x_(n+1)=x_n-f(x_n)/f'(x_n)
def NR(f, fderiv, x0, tol=0.0001, nmax=10, n=1):
	while n<=nmax:
		x1 = x0 - (f(x0)/fderiv(x0))
		if abs(x1 - x0) < tol:
			return x1
		else:
			x0 = x1
	return False

#f(X)=x^3 - x -2
#runs top-level module
if __name__ == "__main__":
	
	def func(x):
	#Function f(x)=x^3 - x -2
	   return x**3-x -2.0 

	def funcderiv(x):
	#Derivative of the function f(x) = x^3 - x -2 is f'(x)=3x^2-1
	   return 3*x**2-1.0
	
	#Caluclating Root using Newton Raphson Method
	root = NR(f=func,fderiv=funcderiv,x0=1)
	print "The root of x**3-x -2.0 is", root
	
#for f(x)=sin(x)
if __name__ == "__main__":
	
	def sinfunc(x):
	#Function f(x)=sin(x)
	   return np.sin(x) 

	def sinderiv(x):
	#Derivate of the function f(x) = sin(x) is f'(x)=cos(x)
	   return np.cos(x)
	
	#Caluclating Root using Newton Raphson Method
	root = NR(f=sinfunc,fderiv=sinderiv,x0=2)
	print "The root of sin(x) is", root
	
	#for f(x)=-6+x+(x**2)
if __name__ == "__main__":
	
	def func2(x):
	#Function f(x)=x**2+x-6
	   return x**2+x-6.0 

	def func2deriv(x):
	#Derivative of the function f(x) = x**2+x-6 is f'(x)=2*x+1
	   return 2*x-1.0
	
	#Caluclating Root using Newton Raphson Method
	root = NR(f=func2,fderiv=func2deriv,x0=1)
	print "The root of x**2+x-6 is", root

#timing the rootfinding
from datetime import datetime
from datetime import timedelta
t1=datetime.now()
t2=datetime.now()
print "The time for the Newton Rapshun method is", t2-t1
print "The time for the bisection method is the same, 00:00:00.000011 seconds"

#Part 2 Finding the Temperature of a Black Body 
#Planck function B(T)=(2hv^3/c^2)/(exp(hv/kT)-1)

h=6.626*10**-27
c=2.9979*10**10
k=1.381*10**-16
lamda=870*10**-4
v=c/lamda
g=(h*v)/k
g_=k/(h*v)

print g


if __name__ == "__main__":
	
	def BBfunc(x):
	#Function f(x)=((2.0*h*v**3)/(c**2))*(1/(expm1(g/x)))-1.25*10**-12 
	   return ((2*h*v**3)/(c**2))*(1/(expm1(g/x)))-1.25*10**-12 
	   
	def BBderiv(x):
	#Derivative of the function f'(x)=((2.0*h*v**3)/(c**2))*((h*v*(exp(g/x)))/((k*x**2)*(expm1(g/x))**2))
	   return ((2*h*v**3)/(c**2))*((h*v*(exp(g/x)))/((k*x**2)*(expm1(g/x))**2))
	
	#Calculating Root using Newton Raphson Method
	root = NR (f=BBfunc,fderiv=BBderiv,x0=40)
	print "The temperature in Kelvin is", root

      

    
        
        
        
        
        