import numpy as numpy
from numpy import random
from math import log

#set up a 2D room with 100 people placed at random x,y locations in 2x2 room
M=100 #100 people in the room
x0_position=1.0*numpy.random.random()-1
y0_position=1.0*numpy.random.random()-1
def position(M):
    count=0 
    for person in range (0, M):
        if (x0_position*y0_position<=1):
            count=count+1
    return count
print "The number of people randomly distributed in the 2x2 room centered at the origin is", position(M)

        
#Have a clock going through time steps. Every time step, move the people
#in random directions for both x and y
tmax=100
t=0
def time_steps(t):
    if t<=tmax:
        t=t+1
        return True
    else: return False
print time_steps(100)

def rand_walk(M):
    for person in range(M):
        walk_x=0.1*(numpy.random.random()-1)
        walk_y=0.1*(numpy.random.random()-1)
        if time_steps(t)==True and x0_position<=1 and x0_position>=-1:
            x_position=x0_position+walk_x
        else:
            x_position=x0_position-walk_x
        if time_steps(t)==True and y0_position<=1 and y0_position>=-1:
            y_position=y0_position+walk_y
        else:
            y_position=y0_position-walk_y
    return x_position, y_position
print "Random walk position", rand_walk(M)

#write a function that can draw random numbers from an exponential distribution"""
"""def exp_dist(M):
    z=numpy.random.exponential()
    return z
print exp_dist(M)"""

x=numpy.random.random()
y=100
def expon_dist(y):
    for chance in range(y):
        y=log(1-x)
print expon_dist(y)
    
    
     