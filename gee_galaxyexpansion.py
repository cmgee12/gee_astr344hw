#Astro 344b expansion simulation
import numpy as numpy
from numpy import random
from math import pi
from math import exp
from math import sqrt
from numpy import float64


#parameters
om_m=0.3
om_lam=0.7
c=3*10**8
n=64 #128 number of particles
G=6.67*10**-11

H_0= 70.*3.24*10**(-20) #[1/s]
a_mlam=(om_m/om_lam)**(1/3) #Ryden Eq 6.27 ~.75
z=250 #a=1/(1+z) so corresponding a_0=1/250, a_f=1/7

#calculate mass of each particle
density=((1.4*10**11)*(2*10**30))/(3.086*10**22)**3 #density in kg/m^3
print "density", density
M=density*(3.086*10.**23.)**3.
m=(M/128.)
print "mass of each particle", m #mass of each particle in kg

#a(t) expansion of universe
t=3.5*10**6 #years
t_0=14.0*10**9 #years
def a(t):
    a= (t/t_0)**(2./3.)#((3/2)*sqrt(om_m)*H_0*t)**(2/3) assuming matter dominated universe
    #a=a_mlam*exp(sqrt(1.-om_m)*H_0*t) #Ryden Cosmology Eq 6.30
    return a


#Particles randomly distributed in a 10 Mpc Box=3.086*10**23m
"""x_position= [  8.63062807e+21,   2.30381971e+22,   1.35551157e+23,   1.96946714e+23,
   2.96516125e+23,   8.93302686e+22,   2.43094604e+23,   1.46596949e+23,
   2.24147641e+22,   2.83053996e+23,   2.09093088e+23,   1.19420578e+22,
   2.02381679e+22 ,  2.11053273e+23,   5.79185581e+22,   2.19071073e+22,
   1.93869532e+23  , 7.67927340e+22,   9.23475294e+22 ,  5.03619510e+22,
   2.18911555e+23   ,6.13861310e+22,   9.13895367e+22 ,  4.56937319e+22,
   2.06405296e+23   ,8.95233445e+21,   2.27797843e+23,   9.07737756e+22,
   4.46719812e+22,   4.30429410e+22,   2.91195036e+23 ,  1.18154858e+23,
   2.47132214e+23 ,  2.63260330e+23,   1.58969221e+23 ,  3.07338470e+23,
   1.26515080e+23 ,  2.33726925e+23,   2.72827955e+23  , 2.25860080e+23,
   3.64272529e+22  , 2.72310530e+22,   2.75561074e+23  , 5.70700791e+22,
   1.22026043e+23 ,  1.94255636e+23,   2.14608440e+23 ,  2.14355830e+23,
   1.42319652e+23 ,  1.52255246e+23,   8.40013484e+22  , 1.34789612e+23,
   6.49322157e+22  , 2.17576845e+22,   6.03718327e+22 ,  2.21935439e+23,
   1.06366179e+23  , 2.72570185e+23,   9.60468689e+22  , 1.06069606e+23,
   4.19000396e+22  , 1.87971790e+23,   2.31842389e+23 ,  2.95547479e+23]"""
x_position=3.086*10**23*numpy.random.random(n)-1.543*10**23
y_position=3.086*10**23*numpy.random.random(n)-1.543*10**23
z_position=3.086*10**23*numpy.random.random(n)-1.543*10**23

print "x positions of n particles:", x_position

def r_x(x_position): #x distance between particles    
    hold=[]
    for particle in range(n):
        x=numpy.arange(n)
        x_position[particle]=float(x_position[particle])
        value=numpy.roll(x,-1)
        while (value[0]<n-1):
            value=numpy.roll(value,-1)
            ind=value[0]
        hold.append(x_position[particle]-x_position[ind])
    return hold
#print "distance from particle n", r_x(x_position)

def r_y(y_position): #y distance between particles    
    hold=[]
    for particle in range(n):
        y=numpy.arange(n)
        y_position[particle]=float(y_position[particle])
        value=numpy.roll(y,-1)
        while (value[0]<n-1):
            value=numpy.roll(value,-1)
            ind=value[0]
        hold.append(y_position[particle]-y_position[ind])
    return hold
    
def r_z(z_position): #x distance between particles    
    hold=[]
    for particle in range(n):
        z=numpy.arange(n)
        z_position[particle]=float(z_position[particle])
        value=numpy.roll(z,-1)
        while (value[0]<n-1):
            value=numpy.roll(value,-1)
            ind=value[0]
        hold.append(z_position[particle]-z_position[ind])
    return hold  
            
#force and acceleration of interacting particles
def acc_x(r_x, a): #total acceleration of each individual particle
    hold=[]
    for particle in range(n):
        if r_x[particle]<0: #acceleration in -x direction
            hold.append(-((G*m)/(((a*r_x[particle])**2)+1.2*10**21)))
        elif r_x[particle]>0: #acceleration in +x direction
            hold.append((G*m)/(((a*r_x[particle])**2)+1.2*10**21))
        else: 
            hold.append(0)
    return numpy.sum(hold) #total force on 1 particle
    
def acc_y(r_y, a): #total acceleration of each individual particle
    hold=[]
    for particle in range(n):
        if r_y[particle]<0: #acceleration in -x direction
            hold.append(-((G*m)/(((a*r_y[particle])**2)+1.2*10**21)))
        elif r_y[particle]>0: #acceleration in +x direction
            hold.append((G*m)/(((a*r_y[particle])**2)+1.2*10**21))
        else: 
            hold.append(0)
    return numpy.sum(hold) #total force on 1 particle

def acc_z(r_z, a): #total acceleration of each individual particle
    hold=[]
    for particle in range(n):
        if r_z[particle]<0: #acceleration in -x direction
            hold.append(-((G*m)/(((a*r_z[particle])**2)+1.2*10**21)))
        elif r_z[particle]>0: #acceleration in +x direction
            hold.append((G*m)/(((a*r_z[particle])**2)+1.2*10**21))
        else: 
            hold.append(0)
    return numpy.sum(hold) #total force on 1 particle

                
x_accels=[] #total x accelerations, each element is acceleration of different particle        
def x_acctot(x_position, a):
    for particles in range(n):
    #find distance between particle and every other particle, convert to acceleration
       #find distance between particle and every other particle
        x_position=numpy.roll(x_position,-1)
        #print "distances from particle n", r_x(x_position)
        r_xval= r_x(x_position)
        acc_xval=acc_x(r_xval, a)
        #print "total force acc_x on single particle", acc_x #total force from each particle on particle n
        #x_accels.append(numpy.sum(acc_x)) #total x acceleration of a single particle
        #print "net x acceleration", x_accels
        x_accels.append(acc_xval) #not appending to list
    #print "All x accelerations", x_accels #total x acceleration of each particle
    return x_accels
    
y_accels=[] #total x accelerations, each element is acceleration of different particle        
def y_acctot(y_position, a):
    for particles in range(n):
    #find distance between particle and every other particle, convert to acceleration
       #find distance between particle and every other particle
        y_position=numpy.roll(y_position,-1)
        #print "distances from particle n", r_x(x_position)
        r_yval= r_y(y_position)
        acc_yval=acc_y(r_yval, a)
        #print "total force acc_x on single particle", acc_x #total force from each particle on particle n
        #x_accels.append(numpy.sum(acc_x)) #total x acceleration of a single particle
        #print "net x acceleration", x_accels
        y_accels.append(acc_yval) #not appending to list
    #print "All x accelerations", x_accels #total x acceleration of each particle
    return y_accels
    
z_accels=[] #total x accelerations, each element is acceleration of different particle        
def z_acctot(z_position, a):
    for particles in range(n):
    #find distance between particle and every other particle, convert to acceleration
       #find distance between particle and every other particle
        z_position=numpy.roll(z_position,-1)
        #print "distances from particle n", r_x(x_position)
        r_zval= r_z(z_position)
        acc_zval=acc_z(r_zval, a)
        #print "total force acc_x on single particle", acc_x #total force from each particle on particle n
        #x_accels.append(numpy.sum(acc_x)) #total x acceleration of a single particle
        #print "net x acceleration", x_accels
        z_accels.append(acc_zval) #not appending to list
    #print "All x accelerations", x_accels #total x acceleration of each particle
    return z_accels 
 

#create array with velocities of particles
x_vel=numpy.zeros(n)
y_vel=numpy.zeros(n)
z_vel=numpy.zeros(n)

#velocity of particles v=v_0+acc*t
def x_velocity(x_vel, x_acctot, t): #v_f=v_i+a*t
    for particle in range(n):
        x_vel[particle]=float(x_vel[particle])
        x_acctot[particle]=float(x_acctot[particle])
        new_velx=x_vel[particle]+x_acctot[particle]*t
        x_vel[particle]=new_velx
    #print "x velocity of particle", x_vel   
    return x_vel
    
def y_velocity(y_vel, y_acctot, t): #v_f=v_i+a*t
    for particle in range(n):
        y_vel[particle]=float(y_vel[particle])
        y_acctot[particle]=float(y_acctot[particle])
        new_vely=y_vel[particle]+y_acctot[particle]*t
        y_vel[particle]=new_vely
    #print "x velocity of particle", x_vel   
    return y_vel
    
def z_velocity(z_vel, z_acctot, t): #v_f=v_i+a*t
    for particle in range(n):
        z_vel[particle]=float(z_vel[particle])
        z_acctot[particle]=float(z_acctot[particle])
        new_velz=z_vel[particle]+z_acctot[particle]*t
        z_vel[particle]=new_velz
    #print "x velocity of particle", x_vel   
    return z_vel


#move particle position x=x0+vt+(1/2)at^2
def x_move(x_position, x_vel, x_acctot,t):
    for particle in range(n):
        x_position[particle]=float(x_position[particle])
        x_vel[particle]=float(x_vel[particle])
        x_acctot[particle]=float(x_acctot[particle])
        new_x_position=x_position[particle]+(x_vel[particle]*t)+(.5*x_acctot[particle]*t**2)
        x_position[particle]=new_x_position
    #print "new x position of particle", x_position
    return x_position
#print "new x positions of particles", x_move(x_position, x_vel, x_acctot)

def y_move(y_position, y_vel, y_acctot,t):
    for particle in range(n):
        y_position[particle]=float(y_position[particle])
        y_vel[particle]=float(y_vel[particle])
        y_acctot[particle]=float(y_acctot[particle])
        new_y_position=y_position[particle]+(y_vel[particle]*t)+(.5*y_acctot[particle]*t**2)
        y_position[particle]=new_y_position
    #print "new x position of particle", x_position
    return y_position
    
def z_move(z_position, z_vel, z_acctot,t):
    for particle in range(n):
        z_position[particle]=float(z_position[particle])
        z_vel[particle]=float(z_vel[particle])
        z_acctot[particle]=float(z_acctot[particle])
        new_z_position=z_position[particle]+(z_vel[particle]*t)+(.5*z_acctot[particle]*t**2)
        z_position[particle]=new_z_position
    #print "new x position of particle", x_position
    return z_position
    
for t in range(1):
     while t<7.6*10**8:
        t=t+1.0*10**6
        val1 = a(t)
        list1 = x_acctot(x_position, val1)
        list1y = y_acctot(y_position, val1)
        list1z = z_acctot(z_position, val1)
        list2 = x_velocity(x_vel, x_position, val1)
        list2y = y_velocity(y_vel, y_position, val1)
        list2z = z_velocity(z_vel, z_position, val1)
        list3 = x_move(x_position, list2, list1, val1)
        list3y = y_move(y_position, list2y, list1y, val1)
        list3z = z_move(z_position, list2z, list1z, val1)
        print "time", t
        print "x positions", list3
        print "y positions", list3y
        print "z positions", list3z
print "final x postions of particles", x_move
print "final y positions of particles", y_move
print "final z positions of particles", z_move

#plot particle positions
import matplotlib.pyplot as plt
data=plt.plot(x_position, y_position, 'bo', label='XY Particle Positions')
plt.ylabel('Y')
plt.xlabel('X')
plt.axis([-1.543*10**23, 1.543*10**23, -1.543*10**23, 1.543*10**23])
plt.legend(numpoints=1)
plt.show()
