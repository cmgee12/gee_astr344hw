#Use a Monte Carlo Experiment to answer the following question: 
#What is the smallest number of people in a room for the probability 
#to be greater than 0.5 that two people in the group have the same birthday? 
#(note, the answer is 23 people).

import numpy as numpy
from numpy import random
#from math import factorial

"""

N=1000
I=0        
for i in range(0,N):
    I += (2.0/N) * g(2*numpy.random.random()) #multiply random by 2 so random numbers are from 0-2
print I"""

"""N=365
while i<N:
    i= i*(factorial(365)/((365**numpy.random.random())*factorial(365)))
print i

N=365 #number of people-1 that would have 100% chance of having same birthdays
N_0=365 #365 days in a year
prob=0"""


"""#calculating based on probabilities
N=23
prob=1.0
for i in range(N):
    prob=prob*(365-i)/365
print "probability of shared birthdays", 1-prob"""

#calculating by counting Monte Carlo
#N is the number of people in the room, set to "TRUE" if 2 people share birthday

def same_bday(N):
    uniq_bday={}
    for people in range(N):
        birthday=random.randint(1,366) #first person's birthday is random day labeled 1-366, including leap year days
        if (uniq_bday.get(birthday,False)==True): #overlapping birthdays
            return True
        else: 
            uniq_bday[birthday]=True 
    return False
#print "2 people have the same birthday:", same_bday(N=23)

N=23 #test that 23 is the number of people for 50% probability
def bday_prob(N):
    darts=1*10**5
    count=0.0
    for num in range(darts):
        if same_bday(N)==True:
            count=count+1
    return 1.0 * count/darts
print "The probability of 2 people having the same birthday out of a group of 23 is", bday_prob(N)
        
#calculate the minimum number of people for at least 50% probability of 2 having the same birthday    
def number_for_same_bday(N):
    for N in range(2, 367): 
        if number_for_same_bday(bday_prob) >= 0.5: 
            return N
        else:
            return False
print "The number of people for 50% probability of 2 having the same birthday is", N



        

    
