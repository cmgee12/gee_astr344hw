#plotting graphs for number of people infected over time

from matplotlib import pyplot as plt
import numpy as np
import matplotlib

#plot of time to total infection as a function of number of people vaccinated
"""v=[0, 1, 2, 10, 20, 30, 40, 50, 60, 70, 80, 90, 98, 100]
t = [15, 20, 18, 23, 29, 34, 40, 46, 46, 52, 54, 57, 61, 64 ]


plt.scatter(v, t, c="g", marker=r'$\clubsuit$',
            label="People")
plt.xlabel("Number of people vaccinated")
plt.ylabel("Time to total infection")
plt.axis([0, 100, 0, 70])
plt.legend(loc=2)
plt.show()

# plot of all people unvaccinated
t=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16, 17, 18,19, 20]
s=[1.4, 4.8, 7.75, 9, 17.8, 18.2, 27, 31, 44.6, 66.4, 79.8, 91, 93.5, 95.2, 94.4, 96, 98, 98.6, 99.5, 99.8]


plt.scatter(t, s, c="g", marker=r'$\clubsuit$',
            label="People")
plt.xlabel("Time Step")
plt.ylabel("Number of people infected")
plt.axis([0, 20, 0, 100])
plt.legend(loc=2)
plt.show()"""

#people unvaccinated, avoiding each other
t=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16, 17, 18, 19, 20]
s=[1.4, 5.3, 2.7, 13.3, 6, 20.33, 22.3, 48.7, 35.6, 60, 61.3, 89, 97, 97.3, 91.6, 98.4, 99.3, 100, 100, 99.3]


plt.scatter(t, s, c="g", marker=r'$\clubsuit$',
            label="People")
plt.xlabel("Time Step")
plt.ylabel("Number of people infected while avoiding each other")
plt.axis([0, 20, 0, 100])
plt.legend(loc=2)
plt.show()