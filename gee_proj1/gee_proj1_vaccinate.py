# vaccinating the population to determine how vaccinations slow the spread of contagious disease
import numpy as numpy
from numpy import random
from math import pi
from numpy import zeros
from numpy import nonzero
from numpy import count_nonzero


#100 people randomly placed in a room
x_position=20*numpy.random.random(100)-10
y_position=20*numpy.random.random(100)-10

#print "x_position", x_position
#print "y_position", y_position


def moving_people_x(x_position):
    for people in range(100):
        x_position[people]=float(x_position[people])
        theta=2*pi*numpy.random.random(100)
        newPosition = x_position[people] + 2*numpy.cos(theta[people])
        if newPosition<=10 and newPosition>=-10:
            x_position[people] = newPosition
        else:
            newPosition = x_position[people]-2*numpy.cos(theta[people])
            x_position[people] = newPosition
    return x_position
#print "New x position", moving_people_x(x_position)


"""for time in range(10):
    moving_people_x(x_position)
    time=time+1
print "moving_people_x", moving_people_x(x_position)
print "time", time"""
          
def moving_people_y(y_position):
    for people in range(100):
        y_position[people]=float(y_position[people])
        #rand_walk_y[people]=float(rand_walk_y[people])
        theta=2*pi*numpy.random.random(100)
        newPosition = y_position[people] + 2*numpy.sin(theta[people])
        if newPosition<=10 and newPosition>=-10:
            y_position[people] = newPosition
        else:
            newPosition = y_position[people]-2*numpy.sin(theta[people])
            y_position[people] = newPosition
    return y_position
#print "Moving in y", moving_people_y(y_position)

        

#create array with health status of 100 healthy people
#1=infected
#0=healthy
health=numpy.zeros(100)
#print health

#infect 1 person
health[0]=1
#print "health", health
spread=nonzero(health)
#print "index of original sick person", spread

#vaccinate people
vac=numpy.zeros(100) #100 people's vacination status, 0= not vacinated, -1=vacinated
#print "vac status", vac
#vac[99]=-1 #vaccinate 1 person
print "vac", vac

def vaccinated(moving_people_x, moving_people_y, health, spread, vac):
    for people in range(100):
        moving_people_x(x_position)[people]=float(moving_people_x(x_position)[people])
        moving_people_y(y_position)[people]=float(moving_people_y(y_position)[people])
        health[people]=float(health[people])
        vac[people]=float(vac[people])
        newSpread=nonzero(health)
        chance=numpy.random.random(100)
        if health[people]==1: #people who are sick remain sick
            health[people]=1
        elif any(((moving_people_x(x_position)[people]-moving_people_x(x_position)[newSpread])**2+(moving_people_y(y_position)[people]-moving_people_y(y_position)[newSpread])**2)**(.5)<=1) and chance[people]>=.75 and vac[people]==-1: #only 25% chance of getting sick if vaccinated
            health[people]=1
        elif any(((moving_people_x(x_position)[people]-moving_people_x(x_position)[newSpread])**2+(moving_people_y(y_position)[people]-moving_people_y(y_position)[newSpread])**2)**(.5)<=1)  and chance[people]>=.25 and vac[people]==0:
            health[people]=1
        else: 
            health[people]=0
    return health
    newSpread.append(nonzero(health))
#print "spread epidemic", vaccinated(moving_people_x,  moving_people_y, health, spread, vac) 
#print nonzero(spread_epidemic)

for time in range(20):
    moving_people_x(x_position)
    moving_people_y(y_position)
    vaccinated(moving_people_x,  moving_people_y, health, spread, vac)
    time=time+1
#print nonzero(vaccinated(moving_people_x,  moving_people_y, health, spread, vac))
print "time", time 
print "health", (health) #should be all 1's since everyone should be infected
print "count", count_nonzero(health)

"""def infect_time(time):
    for time in range(1, 1000):
        if count_nonzero(health)>=99:
            return time
        else: 
            return False         
print "time to total infection", time"""