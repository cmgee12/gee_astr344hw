import numpy as numpy
from numpy import random
from math import pi
from numpy import zeros
from numpy import nonzero

#100 people randomly placed in a room
x_position=20*numpy.random.random(100)-10
y_position=20*numpy.random.random(100)-10

print "x_position", x_position
#print "y_position", y_position


def moving_people_x(x_position):
    for people in range(100):
        x_position[people]=float(x_position[people])
        theta=2*pi*numpy.random.random(100)
        newPosition = x_position[people] + 2*numpy.cos(theta[people])
        if newPosition<=10 and newPosition>=-10:
            x_position[people] = newPosition
        else:
            newPosition = x_position[people]-2*numpy.cos(theta[people])
            x_position[people] = newPosition
    return x_position
print "New x position", moving_people_x(x_position)


"""for time in range(10):
    moving_people_x(x_position)
    time=time+1
print "moving_people_x", moving_people_x(x_position)
print "time", time"""
          
def moving_people_y(y_position):
    for people in range(100):
        y_position[people]=float(y_position[people])
        #rand_walk_y[people]=float(rand_walk_y[people])
        theta=2*pi*numpy.random.random(100)
        newPosition = y_position[people] + 2*numpy.sin(theta[people])
        if newPosition<=10 and newPosition>=-10:
            y_position[people] = newPosition
        else:
            newPosition = y_position[people]-2*numpy.sin(theta[people])
            y_position[people] = newPosition
    return y_position
#print "Moving in y", moving_people_y(y_position)

        
"""def loop_plot_pos(time):
    while time<10:
        plot_positions(moving_people_x, moving_people_y)
        time=time+1
    return plot_positions(moving_people_x, moving_people_y)
print plot_positions(moving_people_x, moving_people_y)"""
    
"""for time in range(10):
    moving_people_y(y_position)
    time=time+1
print "moving_people_y", moving_people_y(y_position)
print "time", time """

#create array with health status of 100 healthy people
#1=infected
#0=healthy
health=numpy.zeros(100)
#print health

#infect 1 person
health[0]=1
#print "health", health
spread=nonzero(health)
#print "index of original sick person", spread
                
#spread epidemic
"""def epidemic(x_position, y_position, health, spread):
    for people in range(100):
        health[0]=1
        x_position[people]=float(x_position[people])
        y_position[people]=float(y_position[people])
        health[people]=float(health[people])
        chance=numpy.random.random(100)
        if (((x_position[people]-x_position[0])**2+(y_position[people]-y_position[0])**2)**(.5))<=2 and chance[people]>=.5:
            health[people]=1
        else:
            health[people]=0
    return health
print "epidemic", epidemic(x_position, y_position, health, spread)
epidemic=epidemic(x_position, y_position, health, spread)
spread1=nonzero(epidemic)
print spread1"""

def spread_epidemic(moving_people_x, moving_people_y, health, spread):
    for people in range(100):
        moving_people_x(x_position)[people]=float(moving_people_x(x_position)[people])
        moving_people_y(y_position)[people]=float(moving_people_y(y_position)[people])
        health[people]=float(health[people])
        newSpread=nonzero(health)
        chance=numpy.random.random(100)
        if health[people]==1: #people who are sick remain sick
            health[people]=1
        elif any(((moving_people_x(x_position)[people]-moving_people_x(x_position)[newSpread])**2+(moving_people_y(y_position)[people]-moving_people_y(y_position)[newSpread])**2)**(.5)<=1)  and chance[people]>=.25:
            health[people]=1
        else: 
            health[people]=0
    return health
    newSpread.append(nonzero(health))
print "spread epidemic", spread_epidemic(moving_people_x,  moving_people_y, health, spread) 
#print nonzero(spread_epidemic)

for time in range(15):
    moving_people_x(x_position)
    moving_people_y(y_position)
    spread_epidemic(moving_people_x,  moving_people_y, health, spread)
    time=time+1
print nonzero(spread_epidemic(moving_people_x,  moving_people_y, health, spread))
print time 
print "health", health#should be all 1's since everyone should be infected


"""import matplotlib.pyplot as plt
def plot_positions(moving_people_x, moving_people_y, health):
    for people in range(100):
        plt.plot(moving_people_x[people], moving_people_y[people], 'ro' if health[people]==0 else 'bo', label='infected')
        plt.ylabel('y position')
        plt.xlabel('x position')
        plt.axis([-10, 10, -10, 10])
        plt.legend(numpoints=1)
        plt.show()
print plot_positions(moving_people_x, moving_people_y, health)"""                        

"""#vaccinate people
vac=numpy.zeros(100) #100 people's vacination status, 0= not vacinated, -1=vacinated
#print "vac status", vac
vac[99]=-1 #vaccinate 1 person

def vaccinate(moving_people_x, moving_people_y, health, spread, vac):
    for people in range(100):
        moving_people_x[people]=float(moving_people_x[people])
        moving_people_y[people]=float(moving_people_y[people])
        health[people]=float(health[people])
        vac[people]=float(vac[people])
        chance=numpy.random.random(100)
        newSpread=spread
        if health[people]==1: #people who are sick remain sick
            health[people]=1
        elif any(((moving_people_x[people]-moving_people_x[newSpread])**2+(moving_people_y[people]-moving_people_y[newSpread])**2)**(.5)<=5) and chance[people]>=.75 and vac[people]==-1: #only 25% chance of getting sick if vaccinated
            health[people]=1
        elif any(((moving_people_x[people]-moving_people_x[newSpread])**2+(moving_people_y[people]-moving_people_y[newSpread])**2)**(.5)<=5)  and chance[people]>=.25: #75% chance of getting sick if not vaccinated
            health[people]=1
        else: 
            health[people]=0
        return health
print "1 person vaccinated", vaccinate(moving_people_x, moving_people_y, health, spread, vac)
vaccinate=vaccinate(moving_people_x, moving_people_y, health, spread, vac)
print nonzero(vaccinate)"""

#increase number of vaccinated people
#vaccines=0
#def vaccines_loop():
#    while vaccines<100:
#        vaccinate
#        vaccines=vaccines+1

"""def vaccines(moving_people_x, moving_people_y, health, spread, vac):
    count=0
    while count<=98:
        count=count+1
        print count-1
        vac[count]=-1
        print vac[count]
    #return vac[count]
    for people in range(count):
        moving_people_x[people]=float(moving_people_x[people])
        moving_people_y[people]=float(moving_people_y[people])
        health[count]=float(health[count])
        vac[count]=float(vac[count])
        chance=numpy.random.random(100)
        newSpread=spread
        if health[people]==1: #people who are sick remain sick
            health[people]=1
        elif any(((moving_people_x[people]-moving_people_x[newSpread])**2+(moving_people_y[people]-moving_people_y[newSpread])**2)**(.5)<=5) and chance[people]>=.75 and vac[people]==-1: #only 25% chance of getting sick if vaccinated
            health[count]=1
        elif any(((moving_people_x[people]-moving_people_x[newSpread])**2+(moving_people_y[people]-moving_people_y[newSpread])**2)**(.5)<=5) and chance[people]>=.25: #75% chance of getting sick if not vaccinated
            health[people]=1
        else: 
            health[people]=0
    return health
print vaccines(moving_people_x, moving_people_y, health, spread, vac)"""