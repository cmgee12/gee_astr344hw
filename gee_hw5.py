#ASTR344 HW 5 Rootfinding
import numpy as np

def f(x):
    return np.sin(x)

"""x1=2
x2=4

tol=0.0001
def bisection(x):
    if x1/x2<0:
        x1=x1*1.01
    elif x1/x2>0:"""
        

    


def bisection(eq, seg, tolerance):
    #a=seg['a']
    #b=seg['b']
    a=2
    b=4
   # Fa, Fb = eq(a), eq(b)
    #if Fa*Fb>0:
     #   raise Exception 
    while (b-a > tolerance):
        x=(a+b)/2.0
        f=eq(x)
        if f*eq(a) > 0: a=x
        else: b=x
    return x
print "The root of sin(x) is", bisection(f,{'a':2, 'b':4}, 0.00001)


def g(x):
    return ((x**3)-x-2)

def bisection(eq, seg, tolerance):
    a, b= seg['a'], seg['b']
    Fa, Fb = eq(a), eq(b)
    while (b-a > tolerance):
        x=(a+b)/2.0 #bisection step
        f=eq(x)
        if f*Fa > 0: a=x
        else: b=x
    return x
print "The root of ((x**3)-x-2) is", bisection(g,{'a':1, 'b':2}, 0.00001)

def y(x):
    return (-6+x+(x**2))

def bisection(eq, seg, tolerance):
    a, b= seg['a'], seg['b']
    Fa, Fb = eq(a), eq(b)
    while (b-a > tolerance):
        x=(a+b)/2.0
        f=eq(x)
        if f*Fa > 0: a=x
        else: b=x
    return x
print "The root of (-6+x+(x**2)) is", bisection(y,{'a':0, 'b':5}, 0.000000001)

import matplotlib.pyplot as plt
x=np.arange(0,5, 0.01)
data=plt.plot(x, f(x), 'ro', label='sin(x)')
data1=plt.plot(x, y(x), 'go', label='(-6+x+(x**2))')
data2=plt.plot(x, g(x), 'bo', label='((x**3)-x-2)')
plt.ylabel('y(x)')
plt.xlabel('x')
plt.axis([0, 5, 0, 5])
plt.legend(numpoints=1)
plt.show()
