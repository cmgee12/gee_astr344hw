#ASTR344 Homework 3-differentiation
#write a code to convert the absolute model number counts to differential counts, 
#and plot the observed data against the model

import numpy as np

L_m=[] #luminosity model
N_m=[] #number of galaxies above that luminosity (N>L) model
L_d=[] #logarithm of luminosity from observed data
dN_dL=[] #logarithm of dN/dL from observed data

#read in model data from model_smg.dat
for index, line in enumerate(open('/Users/Carolyn/Desktop/gee_astr344hw/astr344_homework_materials/model_smg.dat')):
    L_m.append(line.split()[0]) #luminosity from column 0
    N_m.append(line.split()[1]) #number of galaxies above luminosity from column 1
#print L_m
#print N_m

#read in observed data from ncounts_850.dat
for index, line in enumerate(open('/Users/Carolyn/Desktop/gee_astr344hw/astr344_homework_materials/ncounts_850.dat')):
    L_d.append(line.split()[0]) #log luminosity data from column 0
    dN_dL.append(line.split()[1]) #log of dN/dL
#print L_d
#print dN_dL

#convert luminosity function to logarithm
from math import log10
def log_L_m(L_m):
    hold=[]
    for value in range(1, len(L_m)-1):
        L_m[value] = float(L_m[value])
        hold.append(log10(L_m[value]))
    return hold
#print log_L_m(L_m)
log_L_m=log_L_m(L_m)

#calculate h1: h1=x_i - x_1-1
def h1(L_m):
    hold=[]
    for value in range(1, len(L_m)):
        L_m[value]=float(L_m[value])
        L_m[value-1]=float(L_m[value-1])
        hold.append(L_m[value]-L_m[value-1])
    return hold
#print np.float64(h1(L_m))
h1=h1(L_m)

#calculate h2: h2=x_i+1 - x_i
def h2(L_m):
    hold=[]
    for value in range(0, len(L_m)-1):
        L_m[value]=float(L_m[value])
        L_m[value+1]=float(L_m[value+1])
        hold.append(L_m[value+1]-L_m[value])
    return hold
#print np.float64(h2(L_m))
h2=np.float64(h2(L_m))

#calculate dN/dL of model using Taylor expansion
def deriv_N_d(N_m, L_m, h1, h2):
    hold=[]
    for value in range(1, len(N_m)-1): #need to subtract 1 from value because of i=1
        L_m[value] = float (L_m[value])
        N_m[value+1]=float(N_m[value+1])
        N_m[value-1]=float(N_m[value-1])
        N_m[value] = float(N_m[value])
        h1[value-1]= float(h1[value-1])
        h2[value]=float(h2[value])
        hold.append(((h1[value-1]*N_m[value+1])/(h2[value]*(h1[value-1]+h2[value])))-((N_m[value]*(h1[value-1]-h2[value]))/(h1[value-1]*h2[value]))-((N_m[value-1]*h2[value])/(h1[value-1]*(h1[value-1]+h2[value]))))
    return hold
#print np.float64(deriv_N_d(N_m, L_m, h1, h2))
deriv_N_d=deriv_N_d(N_m, L_m, h1, h2)

#convert dN/dL to log format
def log_dN_dL(deriv_N_d):
    hold=[]
    for value in range(0, len(N_m)-2):
        deriv_N_d[value]=float(deriv_N_d[value])
        hold.append(log10((abs(deriv_N_d[value]))))
    return hold
#print np.float64(log_dN_dL(deriv_N_d))
log_dN_dL=np.float64(log_dN_dL(deriv_N_d))

#plot data and simulation
import matplotlib.pyplot as plt
model= plt.plot(L_d, dN_dL, 'ro', label='model')
data=plt.plot(log_L_m, log_dN_dL, 'bo', label='data')
plt.ylabel('log(dN/dL)')
plt.xlabel('log(luminosity)')
plt.axis([-1, 3.5, -5, 7])
plt.legend(numpoints=1)
plt.show()
#plt.savefig('astr344hw3.jpg')




