#Astro 344b expansion simulation
import numpy as numpy
from numpy import random
from math import pi
from math import exp
from math import sqrt
from numpy import float64


#parameters
om_m=0.3
om_lam=0.7
c=3*10**8
n=3 #128 number of particles
G=6.67*10**-11 #S.I. units

H_0= 70.*3.24*10**(-20) #[1/s]
a_mlam=(om_m/om_lam)**(1/3) #Ryden Eq 6.27 ~.75
z=250 #a=1/(1+z) so corresponding a_0=1/251, a_f=1/7

#calculate mass of each particle
density=((1.4*10**11)*(2*10**30))/(3.086*10**22)**3 #density in kg/m^3
print "density", density
M=density*(3.086*10.**23.)**3.
m=M/128.
print "mass of each particle", m #mass of each particle in kg


#a(t) expansion of universe
t=1.0*10**8 #years
t_0=14.0*10**9 #years
def a():
    a= (t/t_0)**(2./3.)#((3/2)*sqrt(om_m)*H_0*t)**(2/3) assuming matter dominated universe
    #a=a_mlam*exp(sqrt(1.-om_m)*H_0*t) #Ryden Cosmology Eq 6.30
    return a
print "a(t)", a()
a=a()

#Particles randomly distributed in a 10 Mpc Box=3.086*10^23m
x_position=[3.01866971e+23,   2.32238437e+23,   1.36664085e+23]#3.086*10**23*numpy.random.random(n)
y_position=10*numpy.random.random(n)
z_position=10*numpy.random.random(n)

print "x positions of n particles:", x_position

"""#roll index to compare distances
x=numpy.arange(n)
value=numpy.roll(x,-1)
while (value[0]<n-1):
    #print "value", value
    value=numpy.roll(value,-1)
    print "value", value
    print "value 0 index:", value[0]
ind=value[0]"""

"""#find distance between particle and every other particle
def r_x(x_position): #x distance between particles   
    hold=[]
    for particle in range(n):
        x=numpy.arange(n)
        x_position[particle]=float(x_position[particle])
        value=numpy.roll(x,-1) #index numbers
        while (value[0]<n-1):
            value=numpy.roll(value,-1)
            ind=value[0]
        hold.append(x_position[particle]-x_position[ind])
    return hold
print "distances from particle n", r_x(x_position) #distance from every particle to nth particle
r_x=r_x(x_position)
print "r_x is", r_x

def acc_x(r_x): #total acceleration of each individual particle
    hold=[]
    for particle in range(n):
        if r_x[particle]<0: #acceleration in -x direction
            hold.append(-((G*m)/(a*r_x[particle])**2))
        elif r_x[particle]>0: #acceleration in +x direction
            hold.append((G*m)/(a*r_x[particle])**2)
        else: 
            hold.append(0)
    return numpy.sum(hold) #total force on 1 particle
acc_x=acc_x(r_x)
print "acc_x of nth particle", acc_x

acceleration=[]
for i in range(n):
    acceleration.append(acc_x(r_x[i]))
print "acceleration", acceleration"""
    
def r_x(x_position): #x distance between particles    
    hold=[]
    for particle in range(n):
        x=numpy.arange(n)
        x_position[particle]=float(x_position[particle])
        value=numpy.roll(x,-1)
        while (value[0]<n-1):
            value=numpy.roll(value,-1)
            ind=value[0]
        hold.append(x_position[particle]-x_position[ind])
    return hold
#print "x distances", r_x(x_position) 
            
#force and acceleration of interacting particles
def acc_x(r_x): #total acceleration of each individual particle
    hold=[]
    for particle in range(n):
        if r_x[particle]<0: #acceleration in -x direction
            hold.append(-((G*m)/(a*r_x[particle])**2))
        elif r_x[particle]>0: #acceleration in +x direction
            hold.append((G*m)/(a*r_x[particle])**2)
        else: 
            hold.append(0)
    return numpy.sum(hold) #total force on 1 particle
                                
x_accels=[] #total x accelerations, each element is acceleration of different particle        
def x_acctot(x_position):
    for particles in range(n):
    #find distance between particle and every other particle, convert to acceleration
       #find distance between particle and every other particle
        x_position=numpy.roll(x_position,-1)
        #print "distances from particle n", r_x(x_position)
        r_xval= r_x(x_position)
        acc_xval=acc_x(r_xval)
        #print "total force acc_x on single particle", acc_x #total force from each particle on particle n
        #x_accels.append(numpy.sum(acc_x)) #total x acceleration of a single particle
        #print "net x acceleration", x_accels
        x_accels.append(acc_xval) #not appending to list
    return x_accels 
print "All x accelerations", x_acctot(x_position) #total x acceleration of each particle
x_acctot=x_acctot(x_position)
    
#create array with velocities of particles
x_vel=numpy.zeros(n)
y_vel=numpy.zeros(n)
z_vel=numpy.zeros(n)

#velocity of particles v=v_0+acc*t
def x_velocity(x_vel, x_acctot): #v_f=v_i+a*t
    for particle in range(n):
        x_vel[particle]=float(x_vel[particle])
        x_acctot[particle]=float(x_acctot[particle])
        new_velx=x_vel[particle]+x_acctot[particle]*t
        x_vel[particle]=new_velx
    return x_vel
print "x velocity of particle", x_velocity(x_vel, x_acctot)
x_velocity=x_velocity(x_vel, x_acctot)

#move particle position x=x0+vt+(1/2)at^2
def x_move(x_position, x_vel, x_acctot):
    for particle in range(n):
        x_position[particle]=float(x_position[particle])
        x_vel[particle]=float(x_vel[particle])
        x_acctot[particle]=float(x_acctot[particle])
        new_x_position=x_position[particle]+(x_vel[particle]*t)+(.5*x_acctot[particle]*t**2)
        x_position[particle]=new_x_position
    return x_position
print "new x positions of particles", x_move(x_position, x_vel, x_acctot)
x_move=x_move(x_position, x_vel, x_acctot)
    
for t in range(1):
     while t<t_0:
        a
        r_x
        acc_x
        x_acctot
        x_velocity
        x_move
        t=t+1.0*10**6
        print "time", t
        print "x positions", x_move
print "final x postions of particles", x_move

#plot particle positions
import matplotlib.pyplot as plt
data=plt.plot(x_position, y_position, 'bo', label='XY Particle Positions')
plt.ylabel('Y')
plt.xlabel('X')
plt.axis([0, 3.086*10**68, 0, 3.086*10**68])
plt.legend(numpoints=1)
plt.show()

"""#a(t) expansion of universe
t=numpy.arange(0,100,10) #maybe set t to a constant?
#a(t) expansion of universe
def expand_universe(t):
    for i in range(n):
        a=a_mlam*exp(sqrt(1-om_m)*H_0*t[i]) #Ryden Cosmology Eq 6.30
    return a
print "a(t) expansion of universe:", expand_universe(t)
a=expand_universe(t)"""